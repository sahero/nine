const express = require('express');
const cors = require('cors')
const compression = require('compression');
const helmet = require('helmet');

const routes = require('../routes');

const server = express();

server.use(compression()); //compress all routes
server.use(helmet()); //protect the app from well-known web vulnerabilities

server.use(express.json());
server.use(express.urlencoded({ extended: false }));
server.use(cors())

server.use(function (err, req, res, next) {
    if (err instanceof SyntaxError && err.status === 400 && "body" in err) {
        res.status(400).send({ "error": "Could not decode request: JSON parsing failed" });
    } else {
        next();
    }
});

server.use('/', routes);

module.exports = server;