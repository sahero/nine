const request = require('supertest')
const app = require('../server')

const mockValidInput = require('../lib/test-news-input.json')
const mockValidOutput = require('../lib/test-news-output.json')

describe('Checking Endpoints', () => {
    describe('Post Endpoints', () => {
        it('should return status code 200 and correct response', async () => {

            const res = await request(app)
                .post('/')
                .send(mockValidInput)
            expect(res.statusCode).toEqual(200)
            expect(res.body).toHaveProperty('response')
            expect(res.body.response).toHaveLength(7)
            expect(res.body).toMatchObject(mockValidOutput)
        })
    })

    describe('Get Endpoints', () => {
        it('should return status code 200 and correct response', async () => {

            const res = await request(app)
                .get('/')
                .send(mockValidInput)
            expect(res.statusCode).toEqual(200)
            expect(res.body).toHaveProperty('response')
            expect(res.body.response).toHaveLength(7)
            expect(res.body).toMatchObject(mockValidOutput)
        })
    })
})