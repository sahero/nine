const { Router } = require('express');
const newsController = require('../controllers/newsController');

const router = Router();

router.route('/').post(newsController.getNews);
router.route('/').get(newsController.getNews);

module.exports = router;
