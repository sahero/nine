
const { Router } = require('express');
const NewsControllers = require('../controllers/newsController');

const router = Router();

router.post('/', NewsControllers.getNews);
router.get('/', NewsControllers.getNews);

/* Invalid routes */
router.get('*', function (req, res) {
    res.status(404).send({ message: `Route ${req.url} not found.` });
});

router.post('*', function (req, res) {
    res.status(404).send({ message: `Route ${req.url} not found.` });
});

module.exports = router;