class ValidationHandler {

    IsValidJSONString(str) {
        try {
            JSON.parse(JSON.stringify(str));
        } catch (e) {
            return false;
        }
        return true;
    }

}

module.exports = ValidationHandler;
