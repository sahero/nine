class RequestHandler {

    sendError(res, err = "Could not decode request: JSON parsing failed") {
        return res.status(400).send({ "error": err })
    }

    sendSuccess(res, data) {
        return res.status(200).send({ response: data })
    }

}

module.exports = RequestHandler;