const ValidationHandler = require('./ValidationHandler');

let vH = new ValidationHandler();
const mockValidJson = `{"payload": [{"key":"value"}, {"key2": "value2"}]}`;
const mockInvalidJson = `{"payload": [{"key":"value"}, {"key2: "value2"}]}`;

describe('Testing validation handler',
  () => {
    it('should return true if valid JSON is provided', () => {
      expect(vH.IsValidJSONString(mockValidJson)).toBe(true);
    })

    it('should return false if invalid JSON is provided', () => {
        expect(vH.IsValidJSONString(mockInvalidJson)).toBe(true);
      })
  })