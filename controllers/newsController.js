const RequestHandler = require('../utils/RequestHandler');
const ValidationHandler = require('../utils/ValidationHandler');

let eH = new RequestHandler();
let vH = new ValidationHandler();

exports.getNews = (req, res, next) => {
    let providedData = req.body;
    let payload = providedData.payload;
    let result;

    //Check it the provided input data is valid JSON or not
    if (payload && vH.IsValidJSONString(payload)) {
        let response = payload
            .filter(news => { return (news.drm && news.episodeCount > 0) })
            .map(news => (
                {
                    "image": news.image ? news.image.showImage : "",
                    "slug": news.slug,
                    "title": news.title
                }
            ))
        result = eH.sendSuccess(res, response)
    }
    else {
        result = eH.sendError(res);
    }

    return result;
}